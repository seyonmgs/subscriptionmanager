
package com.insurance.subscriptionmanager.Service;

import com.insurance.subscriptionmanager.model.Amount;
import com.insurance.subscriptionmanager.model.ApplicationContextProvider;
import com.insurance.subscriptionmanager.model.Subscription;
import com.insurance.subscriptionmanager.model.SubscriptionInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Component
public class SubscriptionService {

    private static List<Subscription> subscriptions = new ArrayList<>();


    public Subscription addSubscription(SubscriptionInfo subscriptionInfo){

        /*System.out.println(subscriptionInfo);*/
        Subscription subscription = new Subscription();
        Amount amount = new Amount(subscriptionInfo.getCost(),"AUD");
        subscription.setAmount(amount);

        subscription.setSubscriptionType(subscriptionInfo.getSubscriptionType());
        subscription.setInvoiceDates(this.getDates(subscriptionInfo));
        subscriptions.add(subscription);

        return subscription;
    }



    public List<LocalDate> getDates (SubscriptionInfo subscriptionInfo){

        LocalDate sDate = subscriptionInfo.getStartDate();
        LocalDate eDate = subscriptionInfo.getEndDate();
        String subscriptionType = subscriptionInfo.getSubscriptionType();
        String day = subscriptionInfo.getDayOfWeek();
        int date = subscriptionInfo.getDateOfMonth();

        Predicate<LocalDate> condition = i -> true;
        long daysBetween = (ChronoUnit.DAYS.between(sDate, eDate) > 90) ? 90 : (ChronoUnit.DAYS.between(sDate, eDate));


        if ((subscriptionType.equalsIgnoreCase("Weekly"))){
            condition = i -> i.getDayOfWeek().toString().equalsIgnoreCase(day);
        }

        if ((subscriptionType.equalsIgnoreCase("Monthly"))){
            condition = i -> i.getDayOfMonth() == date;

        }

        return getDatesConditionally(sDate,daysBetween,condition);
    }


    public List<LocalDate> getDatesConditionally (LocalDate sDate, long daysBetween,
                                                  Predicate<LocalDate> condition ){
        return IntStream.iterate(0, i -> i+1)
                .limit(daysBetween)
                .mapToObj(i -> sDate.plusDays(i))
                .filter(condition)
                .collect(Collectors.toList());
    }


    public List<Subscription> getSubscriptions() {
        return subscriptions;
    }
}
