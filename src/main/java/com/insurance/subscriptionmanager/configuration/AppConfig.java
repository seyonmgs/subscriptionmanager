package com.insurance.subscriptionmanager.configuration;

import com.insurance.subscriptionmanager.Service.SubscriptionService;
import com.insurance.subscriptionmanager.model.ApplicationContextProvider;
import com.insurance.subscriptionmanager.model.Amount;
import com.insurance.subscriptionmanager.model.Subscription;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class AppConfig {
    @Bean
    public ApplicationContextProvider applicationContextProvider(){ return new ApplicationContextProvider();}

}
