
package com.insurance.subscriptionmanager.model;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.validation.constraints.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
public class SubscriptionInfo {

    @NotNull
    @JsonProperty("subscription_type")
    @Pattern(regexp ="^(Monthly|Daily|Weekly)")
    private String subscriptionType;

    @NotNull
    @FutureOrPresent
    @JsonProperty("start_date")
    private LocalDate startDate;

    @NotNull
    @Future
    @JsonProperty("end_date")
    private LocalDate endDate;

    @NotNull
    @PositiveOrZero
    private BigDecimal cost;

    @Positive
    @Max(28)
    @JsonProperty("date_of_month")
    private int dateOfMonth;

    @JsonProperty("day_of_week")
    @Pattern(regexp ="^(Monday|Tuesday|Wednesday|Thursday|Friday|Saturday|Sunday)")
    private String dayOfWeek;

    public SubscriptionInfo(String subscriptionType, LocalDate startDate, LocalDate endDate, BigDecimal cost, int dateOfMonth, String dayOfWeek){
        this();
        this.subscriptionType = subscriptionType;
        this.startDate = startDate;
        this.endDate = endDate;
        this.cost = cost;
        this.dateOfMonth=dateOfMonth;
        this.dayOfWeek=dayOfWeek;
    }

    @Override
    public String toString() {
        return subscriptionType.concat(" ")
                .concat(startDate.toString())
                .concat(" ")
                .concat(endDate.toString()) + " "+ cost + " "
                + dateOfMonth;

    }
}
