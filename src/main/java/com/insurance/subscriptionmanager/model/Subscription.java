package com.insurance.subscriptionmanager.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Getter @Setter
@NoArgsConstructor
public class Subscription {

    private @Setter(AccessLevel.PROTECTED) UUID id;
    private Amount amount;
    @JsonProperty("subscription_type")
    private String subscriptionType;
    @JsonProperty("invoice_dates")
    private List<LocalDate>  invoiceDates;



}
