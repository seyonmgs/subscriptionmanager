package com.insurance.subscriptionmanager.controller;

import com.insurance.subscriptionmanager.Service.SubscriptionService;
import com.insurance.subscriptionmanager.model.Subscription;
import com.insurance.subscriptionmanager.model.SubscriptionInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
public class SubscriptionController {

    private final SubscriptionService subscriptionService;

    @Autowired
    public SubscriptionController (SubscriptionService subscriptionService){
        this.subscriptionService = subscriptionService;
    }

    @PostMapping("/create/subscription")
    public Subscription createSubscription (@Valid @RequestBody SubscriptionInfo subscriptionInfo){
        return subscriptionService.addSubscription(subscriptionInfo);
    }

    @GetMapping("/hello")
    public String getHello(){return "hello";}

    @GetMapping("/subscriptions")
    public List<Subscription> getSubscriptions(){return subscriptionService.getSubscriptions();}
}
