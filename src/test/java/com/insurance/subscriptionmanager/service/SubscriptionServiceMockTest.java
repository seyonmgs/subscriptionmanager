package com.insurance.subscriptionmanager.service;

import com.insurance.subscriptionmanager.Service.SubscriptionService;
import com.insurance.subscriptionmanager.model.ApplicationContextProvider;
import com.insurance.subscriptionmanager.model.SubscriptionInfo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDate;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SubscriptionServiceMockTest {
    @Mock
    SubscriptionInfo subscriptionInfo;

    @Mock
    ApplicationContextProvider applicationContextProvider;

    @InjectMocks
    SubscriptionService subscriptionService;



    @Test
    public void testMonthlyGetDates(){
        subscriptionService = new SubscriptionService(new ApplicationContextProvider());
        when(subscriptionInfo.getSubscriptionType()).thenReturn("Monthly");
        when(subscriptionInfo.getStartDate()).thenReturn(LocalDate.parse("2018-07-02"));
        when(subscriptionInfo.getEndDate()).thenReturn(LocalDate.parse("2018-12-02"));
        when(subscriptionInfo.getDateOfMonth()).thenReturn(25);
        assertEquals(3,subscriptionService.getDates(subscriptionInfo).size());
        assertEquals(LocalDate.parse("2018-07-25"),subscriptionService.getDates(subscriptionInfo).get(0));
        assertEquals(LocalDate.parse("2018-08-25"),subscriptionService.getDates(subscriptionInfo).get(1));
        assertEquals(LocalDate.parse("2018-09-25"),subscriptionService.getDates(subscriptionInfo).get(2));
    }

    @Test
    public void testDailyGetDates(){
        subscriptionService = new SubscriptionService(new ApplicationContextProvider());
        when(subscriptionInfo.getSubscriptionType()).thenReturn("Daily");
        when(subscriptionInfo.getStartDate()).thenReturn(LocalDate.parse("2018-08-02"));
        when(subscriptionInfo.getEndDate()).thenReturn(LocalDate.parse("2018-08-10"));
        assertEquals(8,subscriptionService.getDates(subscriptionInfo).size());
        assertEquals(LocalDate.parse("2018-08-02"),subscriptionService.getDates(subscriptionInfo).get(0));
        assertEquals(LocalDate.parse("2018-08-04"),subscriptionService.getDates(subscriptionInfo).get(2));
        assertEquals(LocalDate.parse("2018-08-09"),subscriptionService.getDates(subscriptionInfo).get(7));
    }


    @Test
    public void testWeeklySubscription(){
        subscriptionService = new SubscriptionService(new ApplicationContextProvider());
        when(subscriptionInfo.getSubscriptionType()).thenReturn("Weekly");
        when(subscriptionInfo.getStartDate()).thenReturn(LocalDate.parse("2018-08-02"));
        when(subscriptionInfo.getEndDate()).thenReturn(LocalDate.parse("2018-09-02"));
        when(subscriptionInfo.getDayOfWeek()).thenReturn("Thursday");
        assertEquals(5,subscriptionService.getDates(subscriptionInfo).size());
        assertEquals(LocalDate.parse("2018-08-02"),subscriptionService.getDates(subscriptionInfo).get(0));
        assertEquals(LocalDate.parse("2018-08-16"),subscriptionService.getDates(subscriptionInfo).get(2));
        assertEquals(LocalDate.parse("2018-08-30"),subscriptionService.getDates(subscriptionInfo).get(4));
    }


}
