package com.insurance.subscriptionmanager;

import com.insurance.subscriptionmanager.model.SubscriptionInfo;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SubscriptionmanagerApplicationTests {

    @LocalServerPort
    private int port;
    TestRestTemplate restTemplate = new TestRestTemplate();
    HttpHeaders headers = new HttpHeaders();

    @Test
    public void postSubscription() throws IOException, JSONException {
        SubscriptionInfo subscriptionInfo = new SubscriptionInfo("Monthly",
                LocalDate.parse("2018-07-02"), LocalDate.parse("2018-11-15")
                , new BigDecimal(699.99)
                , 28
                , "Thursday"
                );

        HttpEntity<SubscriptionInfo> entity = new HttpEntity<SubscriptionInfo>(subscriptionInfo, headers);

        ResponseEntity<String> response = restTemplate.exchange(
                createURLWithPort("/create/subscription"),
                HttpMethod.POST, entity, String.class);


        JSONObject jsonObject = new JSONObject(response.getBody());
        String subType = jsonObject.getString("subscription_type");
        String  value = jsonObject.getJSONObject("amount").getString("value");
        Assert.assertEquals(subscriptionInfo.getSubscriptionType(), subType);
        Assert.assertEquals(subscriptionInfo.getCost().toString().substring(0,6), value);
    }

    private String createURLWithPort(String uri) {
        return "http://localhost:" + port + uri;
    }

}
